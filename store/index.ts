import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import Main from "@/store/Main";

let mainStore: Main

const initializer = (store: Store<any>) => {
    mainStore = getModule(Main, store);
}

export const plugins = [initializer];

export {
    mainStore
}
