import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import { CommentsList } from "@/types/commentsList"
import { Comment } from "@/types/comment"
import { v1 as uuidv1 } from 'uuid';


@Module({
    name: 'Main',
    stateFactory: true,
    namespaced: true,
})
export default class Main extends VuexModule {
    private commentsList: CommentsList = {
        "message": "OK",
        "data": {
            "reviews": {
                "a4515e5d-53b2-4072-acba-c5cf284dd119": {
                    "uuid": "a4515e5d-53b2-4072-acba-c5cf284dd119",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600422939",
                    "changed": "1600422939",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 0,
                    "thread": "00"
                },
                "084afe05-faa2-4746-85bb-214b43ea5543": {
                    "uuid": "084afe05-faa2-4746-85bb-214b43ea5543",
                    "pid": "a4515e5d-53b2-4072-acba-c5cf284dd119",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600422987",
                    "changed": "1600422988",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 1,
                    "thread": "00"
                },
                "69d52571-17e9-4388-a2e0-9e4272e44a6e": {
                    "uuid": "69d52571-17e9-4388-a2e0-9e4272e44a6e",
                    "pid": "084afe05-faa2-4746-85bb-214b43ea5543",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600425481",
                    "changed": "1600425481",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 2,
                    "thread": "00"
                },
                "f6d5bebb-6ac1-4ae0-b3c2-96d4d8b4c448": {
                    "uuid": "f6d5bebb-6ac1-4ae0-b3c2-96d4d8b4c448",
                    "pid": "69d52571-17e9-4388-a2e0-9e4272e44a6e",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600425819",
                    "changed": "1600425819",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 3,
                    "thread": "00"
                },
                "24e914cf-83e1-431e-83d4-4cd28db66d83": {
                    "uuid": "24e914cf-83e1-431e-83d4-4cd28db66d83",
                    "pid": "f6d5bebb-6ac1-4ae0-b3c2-96d4d8b4c448",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600431946",
                    "changed": "1600431946",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 4,
                    "thread": "00"
                },
                "476ebf39-bfbe-474b-a601-a87014a7b69e": {
                    "uuid": "476ebf39-bfbe-474b-a601-a87014a7b69e",
                    "pid": "24e914cf-83e1-431e-83d4-4cd28db66d83",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600432691",
                    "changed": "1600432692",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 5,
                    "thread": "00"
                },
                "370cfb5a-0835-4732-8558-38d54dc1491c": {
                    "uuid": "370cfb5a-0835-4732-8558-38d54dc1491c",
                    "pid": "476ebf39-bfbe-474b-a601-a87014a7b69e",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600432695",
                    "changed": "1600432695",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 6,
                    "thread": "00"
                },
                "37374ca2-4c0c-4852-a966-0b6103045151": {
                    "uuid": "37374ca2-4c0c-4852-a966-0b6103045151",
                    "pid": "370cfb5a-0835-4732-8558-38d54dc1491c",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600689862",
                    "changed": "1600689862",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 7,
                    "thread": "00"
                },
                "9c8b0d9b-bcab-4366-9b2a-33cba190b01d": {
                    "uuid": "9c8b0d9b-bcab-4366-9b2a-33cba190b01d",
                    "pid": "37374ca2-4c0c-4852-a966-0b6103045151",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {
                        "a69cd2e9-a743-4034-a48a-754131b39e9b": {
                            "thumbnail": {
                                "links": {
                                    "fallback": "https://via.placeholder.com/300/FFFF00/000000"
                                }
                            }
                        },
                        "028dd650-6d00-4ff8-8231-986e92edaf2d": {
                            "thumbnail": {
                                "links": {
                                    "fallback": "https://via.placeholder.com/300/FF4F00/004040"
                                }
                            }
                        },
                        "3c7b4a8d-5224-4a74-9cdc-39e86a35adbd": {
                            "thumbnail": {
                                "links": {
                                    "fallback": "https://via.placeholder.com/300/454F06/004040"
                                }
                            }
                        }
                    },
                    "video": {},
                    "created": "1600752289",
                    "changed": "1600752289",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 8,
                    "thread": "00"
                },
                "e3b9ff9e-476b-4e70-bf1c-84245219e46f": {
                    "uuid": "e3b9ff9e-476b-4e70-bf1c-84245219e46f",
                    "pid": "a4515e5d-53b2-4072-acba-c5cf284dd119",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600752261",
                    "changed": "1600752261",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 1,
                    "thread": "01"
                },
                "dd81ddcf-c019-43e6-b525-32eaa3810be5": {
                    "uuid": "dd81ddcf-c019-43e6-b525-32eaa3810be5",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600432704",
                    "changed": "1600432704",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 0,
                    "thread": "01"
                },
                "19bc07c4-1f17-4c24-98ec-077b577dd60e": {
                    "uuid": "19bc07c4-1f17-4c24-98ec-077b577dd60e",
                    "pid": "dd81ddcf-c019-43e6-b525-32eaa3810be5",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600691904",
                    "changed": "1600691904",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 1,
                    "thread": "00"
                },
                "8e8665ad-2aa0-4df1-b6f2-8f611e36f1b0": {
                    "uuid": "8e8665ad-2aa0-4df1-b6f2-8f611e36f1b0",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600449226",
                    "changed": "1600449227",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 0,
                    "thread": "02"
                },
                "e3887c82-84cb-45a0-8957-0da99414b9fb": {
                    "uuid": "e3887c82-84cb-45a0-8957-0da99414b9fb",
                    "pid": "8e8665ad-2aa0-4df1-b6f2-8f611e36f1b0",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600691901",
                    "changed": "1600691901",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 1,
                    "thread": "00"
                },
                "ab718537-95ea-4639-a184-decd332b4fef": {
                    "uuid": "ab718537-95ea-4639-a184-decd332b4fef",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600691896",
                    "changed": "1600691896",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 0,
                    "thread": "03"
                },
                "ca20a6e6-9e27-408f-87b1-1663f6fdf52a": {
                    "uuid": "ca20a6e6-9e27-408f-87b1-1663f6fdf52a",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тестовый комментарий для проверки",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1600764772",
                    "changed": "1600764772",
                    "status": 1,
                    "owner": {
                        "uuid": "7a1b0527-a812-42e5-a1b3-aad6160f8ea5",
                        "login": "zajet1",
                        "firstname": "Иван",
                        "lastname": "Иванов",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/0000FF/808080"
                        }
                    },
                    "step": 0,
                    "thread": "04"
                },
                "93b7c082-b625-4109-a405-fe2ec6477056": {
                    "uuid": "93b7c082-b625-4109-a405-fe2ec6477056",
                    "pid": null,
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "Тест вавы авыаываываываыва ваыа",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1602003038",
                    "changed": "1602003038",
                    "status": 1,
                    "owner": {
                        "uuid": "1d11fb81-1282-4835-889b-16151be0ce94",
                        "login": "Sky_Cat",
                        "firstname": "Петр",
                        "lastname": "Петров",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/FFFF00/000000"
                        }
                    },
                    "step": 0,
                    "thread": "05"
                },
                "ad34400a-79da-447d-80be-3b3669219b53": {
                    "uuid": "ad34400a-79da-447d-80be-3b3669219b53",
                    "pid": "93b7c082-b625-4109-a405-fe2ec6477056",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "ававыавыавыаываsfывавы авыавыа выаываыа",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1602003055",
                    "changed": "1602003055",
                    "status": 1,
                    "owner": {
                        "uuid": "1d11fb81-1282-4835-889b-16151be0ce94",
                        "login": "Sky_Cat",
                        "firstname": "Петр",
                        "lastname": "Петров",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/FFFF00/000000"
                        }
                    },
                    "step": 1,
                    "thread": "00"
                },
                "f33cab39-01e2-46ad-a86e-d8231bfcb799": {
                    "uuid": "f33cab39-01e2-46ad-a86e-d8231bfcb799",
                    "pid": "ad34400a-79da-447d-80be-3b3669219b53",
                    "review_type": "comment",
                    "star": 0,
                    "pros": {
                        "text": "",
                        "format": ""
                    },
                    "cons": {
                        "text": "",
                        "format": ""
                    },
                    "body": {
                        "text": "апавпавпавп ап вап авпав пав пва",
                        "format": "plain_text"
                    },
                    "files": {},
                    "video": {},
                    "created": "1602003090",
                    "changed": "1602003090",
                    "status": 1,
                    "owner": {
                        "uuid": "1d11fb81-1282-4835-889b-16151be0ce94",
                        "login": "Sky_Cat",
                        "firstname": "Петр",
                        "lastname": "Петров",
                        "avatar": {
                            "src": "https://via.placeholder.com/150/FFFF00/000000"
                        }
                    },
                    "step": 2,
                    "thread": "00"
                }
            }
        },
        "status": "SUCCESS"
    };

    private commentsTree: Comment[] = []

    get COMMENTS_LIST(): CommentsList {
        return this.commentsList;
    }

    get COMMENTS_TREE(): Comment[] {
        return this.commentsTree;
    }

    @Mutation
    SET_COMMENTS_TREE(): void {
        console.log('tree rebuilding started')
        let result: Comment[] = [];
        let list = this.commentsList.data.reviews;

        let key: keyof { [key: string]: Comment };
        for (key in list) {
            if (list[key].pid === null) {
                Main.buildTree(list[key], list);
                result.push(list[key]);
            }
        }
        this.commentsTree = result;
    }

    @Mutation
    ADD_COMMENT(comment: Comment) {
        comment.uuid = uuidv1();
        this.commentsList.data.reviews[comment.uuid] = comment;

        let arr: CommentsList = this.commentsList;
        this.commentsList = {} as CommentsList;
        this.commentsList = arr;
    }

    @Mutation
    DELETE_COMMENT(uuid: string) {
        Main.deleteWithChilds(this.commentsList.data.reviews[uuid],
            this.commentsList.data.reviews)

        let arr = this.commentsList;
        this.commentsList = {} as CommentsList;
        this.commentsList = arr;
    }

    private static buildTree(comment: Comment, list: { [key: string]: Comment }): void {
        let key: keyof { [key: string]: Comment };
        comment.answers = [];
        for (key in list) {
            if (list[key].pid === comment.uuid) {
                Main.buildTree(list[key], list);
                comment.answers.push(list[key]);
            }
        }
    }

    private static deleteWithChilds(comment: Comment, list: { [key: string]: Comment }): void {
        if (!!comment.answers) {
            for (let i of comment.answers) {
                Main.deleteWithChilds(i, list)
            }
        }

        delete list[comment.uuid];
    }
}