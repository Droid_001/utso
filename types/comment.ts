export interface TextData {
    text: string,
    format: string,
}

export interface FileItem {
    thumbnail: {
        links: {
            fallback: string
        }
    }
}

export interface Owner {
    uuid: string,
    login: string,
    firstname: string,
    lastname: string,
    avatar: {
        src: string
    }
}

export interface Comment {
    uuid: string,
    status: number,
    star: number,
    pid: string | null,
    review_type: string,
    body: TextData,
    files: { [key: string]: FileItem },
    video: any,
    created: string,
    changed: string,
    owner: Owner,
    step: number,
    pros: TextData,
    cons: TextData,
    thread: string,
    //Для постройки дерева по рекурсии
    answers?: Array<Comment>
}