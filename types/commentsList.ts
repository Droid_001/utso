import { Comment } from "@/types/comment";

export interface CommentsList {
    data: {
        reviews: { [key: string]: Comment },
    },
    status: string,
    message: string
}